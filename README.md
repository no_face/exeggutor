<p align="center">
    <img src="https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/fd1c2946-c6fe-42a1-a26e-830986713a61/d4pnbd8-a7a5ef91-778a-4b82-932c-4d8c28a5d6a3.png/v1/fill/w_800,h_534,q_80,strp/the_exeggcute_family_by_zerochan923600_d4pnbd8-fullview.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9NTM0IiwicGF0aCI6IlwvZlwvZmQxYzI5NDYtYzZmZS00MmExLWEyNmUtODMwOTg2NzEzYTYxXC9kNHBuYmQ4LWE3YTVlZjkxLTc3OGEtNGI4Mi05MzJjLTRkOGMyOGE1ZDZhMy5wbmciLCJ3aWR0aCI6Ijw9ODAwIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmltYWdlLm9wZXJhdGlvbnMiXX0.lQ0NQZNgiXaxjDusMGFre98IVl86qUvsng23F9VR-l8" />
</p>

![npm](https://img.shields.io/npm/v/exeggutor)
![npm](https://img.shields.io/npm/l/exeggutor)
![npm](https://img.shields.io/npm/dm/exeggutor)

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/no_face/exeggutor?branch=master)
![Gitlab code coverage](https://img.shields.io/gitlab/coverage/no_face/exeggutor/master)

# Exeggutor

<p>Exeggutor is a simple, event-driven commandline executor written for nodejs.</p>

<p>Exeggutor lets you subscribe multiple listeners to the stdout and stderr pipes. This allows you to process large outputs in chunks rather than one large blob - avoiding issues regarding maximum length. It's also a nice alternative to complex shell scripting, if the environment already contains NodeJs</p>

# Documentation

## Quick Start

<p>In JavaScript:</p>

```js
const { execute } = require('exeggutor');
await execute('ls', ['-la']);
```

<p>In TypeScript:</p>

```ts
import { execute } from 'exeggutor';
await execute('cat', ['a_really_large_input_file.txt']);
```

## Exeggutor

<p>In some instances, you may want to perform more complex parsing on the outputs of a command.</p>

<p>First, construct an Exeggutor instance to handle events.</p>

<p>In JavaScript:</p>

```js
const { Exeggutor } = require('exeggutor');
const exeggutor = new Exeggutor();
```

<p>In TypeScript:</p>

```ts
import { Exeggutor } from 'exeggutor';
const exeggutor: Exeggutor = new Exeggutor();
```

<p>Next, add one to many event listeners to the stdout and stderr events.</p>

```ts
exeggutor
    .on('stdout', (buf) => {
        /* possibly write to stdout */
    })
    .on('stdout', (buf) => {
        /* possibly parse for keywords/ symbols */
    })
    .on('stderr', (buf) => {
        /* possibly write to stderr */
    });
```
