import { createSandbox } from 'sinon';

import child_process, { ChildProcessWithoutNullStreams } from 'child_process';
import { Exeggutor } from '@src/Exeggutor';

import { getMockSpringBootLogs, getMockChildProcess } from './mocks/Exeggutor.mock';

describe('Exeggutor TestSuite', () => {
    const sandbox = createSandbox();

    const exeggutor = new Exeggutor();
    const expectedLogs = getMockSpringBootLogs();

    afterEach(sandbox.restore);

    it('should successfully add listeners & execute command', async () => {
        const childStub = getMockChildProcess();
        sandbox.stub(child_process, 'spawn').returns(childStub as ChildProcessWithoutNullStreams);
        const stdoutSpy = sandbox.spy();
        const stderrSpy = sandbox.spy();

        const actualLogs: string[] = [];
        exeggutor.on('stdout', (buf: Buffer) => {
            actualLogs.push(buf.toString());
            stdoutSpy();
        });
        exeggutor.on('stderr', (buf: Buffer) => {
            actualLogs.push(buf.toString());
            stderrSpy();
        });

        const execPromise = exeggutor.execute('java', [
            '-jar',
            'mock-springboot-microservice_6.1.8_RELEASE.jar',
        ]);
        expectedLogs.forEach(({ event, log }) => childStub[event].emit('data', Buffer.from(log)));
        childStub.emit('exit');
        await execPromise;

        expect(actualLogs).toEqual(expectedLogs.map(({ log }) => log));
        expect(stdoutSpy.callCount).toEqual(4);
        expect(stderrSpy.callCount).toEqual(1);
    });

    it('should throw Error when failing to execute command', async () => {
        const childStub = getMockChildProcess();
        sandbox.stub(child_process, 'spawn').returns(childStub as ChildProcessWithoutNullStreams);
        const stdoutSpy = sandbox.spy();
        const stderrSpy = sandbox.spy();
        exeggutor.on('stdout', stdoutSpy);
        exeggutor.on('stderr', stderrSpy);

        const expectedError = new Error('some runtime error');
        try {
            const execPromise = exeggutor.execute('java', [
                '-jar',
                'mock-springboot-microservice_6.1.8_SNAPSHOT.jar',
            ]);
            childStub.emit('error', expectedError);
            await execPromise;
        } catch (err) {
            expect(err).toBeInstanceOf(Error);
            expect(err.message).toEqual(expectedError.message);
            expect(stdoutSpy.callCount).toEqual(0);
            expect(stderrSpy.callCount).toEqual(0);
            return;
        }
        fail('Successfully executed ');
    });
});
