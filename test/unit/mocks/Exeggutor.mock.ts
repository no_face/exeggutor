import { EventEmitter } from 'events';

function getMockSpringBootLogs() {
    return [
        {
            event: 'stdout',
            log: '2021-01-01 18:34:18.931 INFO 82754 --- [main] com.path.to.package : Starting some microservice',
        },
        {
            event: 'stdout',
            log: '2021-01-01 18:34:18.931 INFO 82754 --- [main] com.path.to.package : Initializing Executor Thread Pool',
        },
        {
            event: 'stderr',
            log: '2021-01-01 18:34:18.931 WARN 82754 --- [main] com.path.to.package : Failed to database connection (primary us-east-1a)',
        },
        {
            event: 'stdout',
            log: '2021-01-01 18:34:18.931 INFO 82754 --- [main] com.path.to.package : Successfully initialized Executor Thread Pool',
        },
        {
            event: 'stdout',
            log: '2021-01-01 18:34:18.931 INFO 82754 --- [main] com.path.to.package : Initialized database connection (AZ us-east-1b)',
        },
    ];
}

function getMockChildProcess() {
    const childProcess = new EventEmitter();
    childProcess['stdout'] = new EventEmitter();
    childProcess['stderr'] = new EventEmitter();
    return childProcess;
}

export { getMockSpringBootLogs, getMockChildProcess };
