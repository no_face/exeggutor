import { join } from 'path';

import { Exeggutor } from '@src/Exeggutor';

describe('Functional Exeggutor TestSuite', () => {
    const exeggutor = new Exeggutor();

    it('should successfully read from file', async () => {
        const expectedFileContent = 'Contents of file for functional test.';

        const bufferedContent: Buffer[] = [];
        exeggutor
            .on('stdout', (buf) => bufferedContent.push(buf))
            .on('stderr', (buf) => bufferedContent.push(buf));

        await exeggutor.execute('cat', [join(__dirname, 'mock/input.txt')]);

        const actualFileContent = Buffer.concat(bufferedContent).toString();
        expect(actualFileContent).toEqual(expectedFileContent);
    });
});
