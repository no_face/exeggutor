# 1.0.3 / 2021-11-28

### Features

-   Added documentation for `execute` function and `Exeggutor` class

### Fixes

-   `N/A`

# 1.0.2 / 2021-11-27

### Features

-   `N/A`

### Fixes

-   Remove assets from build.

# 1.0.1 / 2021-11-27

### Features

-   `N/A`

### Fixes

-   Publish README.md & assets for official NPM homepage.

# 1.0.0 / 2021-11-27

### Features

-   First major release of `exeggutor@1.0.0`.
-   Export `TypeScript` declaration files.
-   Export `execute` and `Exeggutor` class.

### Fixes

-   `N/A`
