import { stderr, stdout } from 'process';
import { Exeggutor } from '@src/Exeggutor';

const globalExeggutor = new Exeggutor()
    .on('stderr', (buf) => stderr.write(buf))
    .on('stdout', (buf) => stdout.write(buf));

function execute(cmd: string, args?: any[]): Promise<void> {
    return globalExeggutor.execute(cmd, args);
}

export { execute, Exeggutor };
