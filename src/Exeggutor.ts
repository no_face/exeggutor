import { EventEmitter } from 'events';
import { spawn } from 'child_process';

class Exeggutor extends EventEmitter {
    execute(cmd: string, args?: any[]): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            const child = spawn(cmd, args);
            child.on('error', reject);
            child.stdout.on('data', (buf) => this.emit('stdout', buf));
            child.stderr.on('data', (buf) => this.emit('stderr', buf));
            child.on('exit', () => resolve());
        });
    }

    on(event: 'stdout' | 'stderr', listener: (...args: any[]) => void): this {
        return super.on(event, listener);
    }
}

export { Exeggutor };
