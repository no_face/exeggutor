import { EventEmitter } from 'events';

/**
 * The Exeggutor class lets you subscribe multiple listeners to the stdout and stderr pipes
 * similar to an EventEmitter.
 *
 * @extends {EventEmitter}
 */
declare class Exeggutor extends EventEmitter {
    /**
     * This method spawns a new process and executes the given command with provided arguments.
     * @param {string} cmd - the command that the Exeggutor will execute
     * @param {any[]} args - the array of flags and arguments to pass to the executable (command)
     * @returns {Promise<void>} - the Promise that resolves or rejects the state of this asynchronous process
     */
    execute(cmd: string, args?: any[]): Promise<void>;

    /**
     * Adds the listener function to the end of the the event listener array, similar to the EventEmitter.
     * @override
     * @param {'stdout' | 'stderr'} event - the name of the event to register the listener to
     * @param {(...args: any[]) => void} listener - the listener to notify when the event is received
     * @returns {this} - a reference to this Exeggutor instance
     */
    on(event: 'stdout' | 'stderr', listener: (...args: any[]) => void): this;
}

export { Exeggutor };
