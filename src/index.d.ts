import { Exeggutor } from './Exeggutor';

/**
 * This method spawns a new process and executes the given command with provided arguments.
 *
 * This method is a wrapper for an Exeggutor instantiated at the global/ module level. This
 * method automatically subscribes event listeners to the stdout and stderr pipes of the
 * process and outputs the command's results.
 * @param {string} cmd - the command that the Exeggutor will execute
 * @param {any[]} args - the array of flags and arguments to pass to the executable (command)
 * @returns {Promise<void>} - the Promise that resolves or rejects the state of this asynchronous process
 */
declare function execute(cmd: string, args?: any[]): Promise<void>;

export { execute, Exeggutor };
